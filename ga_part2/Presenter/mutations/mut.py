from random import *

from Model.chromosome import Chromosome
from Presenter.crossover.crossover import Cross
from Presenter.function.booth import Booth
from Presenter.generators.population import Population
from Presenter.selection.selection import Selection
from config import Configuration


class Mutation:
    def __init__(self, conf):
        self.conf = conf

    def even_mutation(self, population):
        after_mutation = []
        for parent in population:
            generated_number = uniform(0, 1)
            if self.conf.mutation_probability > generated_number:
                item_for_mutation = [parent.x1_no, parent.x1_no]
                generated_offspring = randint(-10, 10)
                selected_element = randint(0, 1)
                item_for_mutation[selected_element] = generated_offspring
                x1 = item_for_mutation[0]
                x2 = item_for_mutation[1]
                fun = Booth(self.conf.minimize)
                y = fun.function_implementation(x1, x2)
                chrom = Chromosome(x1, x2, y)
                after_mutation.append(chrom)
            else:
                after_mutation.append(parent)
        return after_mutation

    def replace_index_mutation(self, population):
        after_mutation = []
        for parent in population:
            generated_number = uniform(0, 1)
            if self.conf.mutation_probability > generated_number:
                item_for_mutation = [parent.x1_no, parent.x1_no]
                x1 = item_for_mutation[1]
                x2 = item_for_mutation[0]
                fun = Booth(self.conf.minimize)
                y = fun.function_implementation(x1, x2)
                chrom = Chromosome(x1, x2, y)
                after_mutation.append(chrom)
            else:
                after_mutation.append(parent)
        return after_mutation


if __name__ == '__main__':
    conf = Configuration()
    conf.selection_size = 0.3
    conf.population_size = 20
    conf.selection_tournament_size = 5
    conf.minimize = True
    conf.crossing_probability = 0.65
    conf.crossing_heuretic_argument_iterations = 10
    conf.mutation_probability = 0.1
    pop = Population(conf)
    population = pop.generate_population()
    sel = Selection(conf)
    selection = sel.ranking_selection(population)
    selection1 = sel.roulette_selection(population)
    selection3 = sel.tournament_selection(population)
    cross = Cross(conf)
    result_arithmetic_crossing = cross.arithmetic_crossover(population)
    result_heurestic_crossover = cross.heurestic_crossover(population)
    mut = Mutation(conf)
    result_even_mutation = mut.even_mutation(result_arithmetic_crossing)
    result_replace_index_mutation = mut.replace_index_mutation(result_arithmetic_crossing)
    print(population)
