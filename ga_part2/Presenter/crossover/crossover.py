from Model.chromosome import Chromosome
from Presenter.function.booth import Booth
from Presenter.generators.population import Population
from Presenter.selection.selection import Selection
from config import Configuration
from random import *

class Cross:

    def __init__(self, conf):
        self.conf = conf

    def arithmetic_crossover(self, population):
        new_population = []
        for parent in population:
            generated_number = uniform(0,1)
            if generated_number < self.conf.crossing_probability:
                fun = Booth(self.conf.minimize)
                x1 = self.conf.crossing_probability * parent.x1_no + ((1-self.conf.crossing_probability) * parent.x2_no)
                x2 = ((1-self.conf.crossing_probability) * parent.x1_no) + self.conf.crossing_probability * parent.x2_no
                y = fun.function_implementation(x1, x2)
                child = Chromosome(x1, x2, y)
                new_population.append(child)
            else:
                parent.fitness_probability = None
                parent.cumulative_distribution = None
                new_population.append(parent)
        return new_population

    def heurestic_crossover(self, population):
        new_population = []
        for parent in population:
            if self.conf.minimize:
                better_parent = parent.x1_no if (parent.x1_no < parent.x2_no) else parent.x2_no
                worst_parent = parent.x1_no if (parent.x1_no > parent.x2_no) else parent.x2_no
            else:
                better_parent = parent.x1_no if (parent.x1_no > parent.x2_no) else parent.x2_no
                worst_parent = parent.x1_no if (parent.x1_no < parent.x2_no) else parent.x2_no
            offspring_x1, result=self.get_fit_offspring(better_parent, worst_parent)
            if not result:
                new_population.append(parent)
            else:
                offspring_x2=better_parent
                fun = Booth(self.conf.minimize)
                y = fun.function_implementation(offspring_x1, offspring_x2)
                offspring = Chromosome(offspring_x1, offspring_x2, y)
                new_population.append(offspring)
        return new_population

    def get_fit_offspring(self, better_parent, worst_parent):
        for i in range(0, self.conf.crossing_heuretic_argument_iterations):
            generated_number = uniform(0, 1)
            offspring = generated_number * (better_parent-worst_parent) + better_parent
            if offspring > - 10 and offspring < 10:
                return offspring, True
        return 0, False


if __name__ == '__main__':
    conf = Configuration()
    conf.selection_size = 0.3
    conf.population_size = 20
    conf.selection_tournament_size = 5
    conf.minimize = True
    conf.crossing_probability = 0.65
    conf.crossing_heuretic_argument_iterations=10
    pop = Population(conf)
    population = pop.generate_population()
    sel = Selection(conf)
    selection = sel.ranking_selection(population)
    selection1 = sel.roulette_selection(population)
    selection3 = sel.tournament_selection(population)
    cross = Cross(conf)
    result_arithmetic_crossing = cross.arithmetic_crossover(population)
    result_heurestic_crossover = cross.heurestic_crossover(population)
    print(population)
