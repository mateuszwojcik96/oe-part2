from math import *
import numpy as np
import random

from Presenter.generators.population import Population
from config import Configuration


class Selection:

    def __init__(self, conf):
        self.conf = conf

    def ranking_selection(self, population):
        result = []
        size = int(ceil(self.conf.selection_size * len(population)))
        if self.conf.minimize:
            self.sorted_population = sorted(population, key=lambda chr: chr.y)
        else:
            self.sorted_population = sorted(population, key=lambda chr: chr.y, reverse=True)
        number = 0

        while(number < len(population)):
            for i in range(0, size):
                if number == len(population):
                    return result
                result.append(self.sorted_population[i])
                number += 1
        return result


    def roulette_selection(self, population):
        fitting_population = []
        selections_parents = []
        if not self.conf.minimize:
            fitness_sum = sum((a.y) for a in population)
            cumulative_distribution = 0
            for pop in population:
                pop.fitness_probability = ((pop.y) / fitness_sum)
                cumulative_distribution += pop.fitness_probability
                pop.cumulative_distribution = cumulative_distribution
                fitting_population.append(pop)

            drawb_numbers = np.random.random(len(population))
            for number in drawb_numbers:
                selections_parents.append(self.select_from_fitting_population(number, fitting_population))
        else:
            fitness_sum = sum((1 / a.y) for a in population)
            cumulative_distribution = 0
            for pop in population:
                pop.fitness_probability = ((1 / pop.y) / fitness_sum)
                cumulative_distribution += pop.fitness_probability
                pop.cumulative_distribution = cumulative_distribution
                fitting_population.append(pop)

            drawb_numbers = np.random.random(len(population))
            for number in drawb_numbers:
                selections_parents.append(self.select_from_fitting_population(number, fitting_population))
        return selections_parents

    def select_from_fitting_population(self, number, fitting_population):
        for no in fitting_population:
            if number < no.cumulative_distribution:
                return no


    def tournament_selection(self, population):
        result = []

        for i in range(0, len(population)):
            result.append(self.get_best_value_from_tournament(population))

        return result

    def get_best_value_from_tournament(self, population):
        tournament_result = []
        for i in range(0, self.conf.selection_tournament_size):
            tournament_group = []
            tournament_group.append(population[random.randint(0, len(population) - 1)])
            tournament_group.append(population[random.randint(0, len(population) - 1)])
            tournament_group.append(population[random.randint(0, len(population) - 1)])
            if self.conf.minimize:
                tournament_result.append(min(tournament_group, key=lambda x: x.y))
            else:
                tournament_result.append(max(tournament_group, key=lambda x: x.y))
        if self.conf.minimize:
            return min(tournament_result, key=lambda x: x.y)
        return max(tournament_result, key=lambda x: x.y)

if __name__ == '__main__':
    conf = Configuration()
    conf.selection_size = 0.3
    conf.population_size = 20
    conf.selection_tournament_size = 5
    conf.minimize = True
    pop = Population(conf)
    population = pop.generate_population()
    sel = Selection(conf)
    selection = sel.ranking_selection(population)
    selection1 = sel.roulette_selection(population)
    selection3 = sel.tournament_selection(population)
    print(population)