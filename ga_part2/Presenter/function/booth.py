

class Booth:
    def __init__(self, minimize=True):
        self.low_border = -10
        self.high_border = 10
        self.minimize = minimize
        self.name = 'Booth function'

    def function_implementation(self, a, b):
        part1 = (a + 2 * b - 7) ** 2
        part2 = (2 * a + b - 5) ** 2
        return part1 + part2
