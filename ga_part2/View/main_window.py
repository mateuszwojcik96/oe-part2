import tkinter as tk
import random as r
import datetime
import csv
from random import random
from tkinter import ttk
from tkinter import messagebox
from statistics import mean

from Presenter.crossover.crossover import Cross
from Presenter.function.booth import Booth
from Presenter.generators.population import Population
from Presenter.mutations.mut import Mutation
from Presenter.selection.selection import Selection
from config import Configuration


class MainWindow:
    def __init__(self):
        self.gui = tk.Tk()
        self.isMinimize = ''
        self.population_size = ''
        self.eveluation_size = ''
        self.precision = ''
        self.selection = ''
        self.selection_conf = ''

    def show(self):
        self.gui.geometry('800x400')
        self.gui.title('Implementacja algorytmu genetycznego')

        # menu
        self.menubar = tk.Menu(self.gui)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Zatwierdź ustawienia", command=self.confirm_settings)
        self.filemenu.add_command(label="Wykonaj algorytm", command=self.start_algorithm)
        self.filemenu.add_command(label="Pokaż wykres", command=self.confirm_settings)
        self.menubar.add_cascade(label="Opcje", menu=self.filemenu)

        # optimize option
        self.check_variable = tk.IntVar()
        self.isMinimize = tk.Checkbutton(self.gui, text="Minimalizacja", variable=self.check_variable, onvalue=1, offvalue=0,
                                         height=5, width=30).grid(row=0)

        # inputs
        tk.Label(self.gui, text="Liczba populacji").grid(row=1, sticky='W')
        self.population_size = tk.Entry(self.gui, width=30)
        self.population_size.grid(row=1, column=1)

        tk.Label(self.gui, text="Liczba ewolucji").grid(row=2, sticky='W')
        self.eveluation_size = tk.Entry(self.gui, width=30)
        self.eveluation_size.grid(row=2, column=1)


        tk.Label(self.gui, text="Konfiguracja selekcji").grid(row=5, sticky='W')
        self.selection_conf = tk.Entry(self.gui, width=30)
        self.selection_conf.grid(row=5, column=1)

        tk.Label(self.gui, text="Prawdopodobieńśtwo krzyżowania").grid(row=7, sticky='W')
        self.crossing_conf = tk.Entry(self.gui, width=30)
        self.crossing_conf.grid(row=7, column=1)

        tk.Label(self.gui, text="Prawdopodobieńśtwo mutacji").grid(row=9, sticky='W')
        self.mutation_conf = tk.Entry(self.gui, width=30)
        self.mutation_conf.grid(row=9, column=1)

        # tk.Label(self.gui, text="Strategia elitarna").grid(row=10, sticky='W')
        # self.count_members = tk.Entry(self.gui, width=30)
        # self.count_members.grid(row=10, column=1)

        # selection
        tk.Label(self.gui, text="Metoda selekcji").grid(row=4, sticky='W')
        self.cb_value = tk.StringVar()
        self.selection = ttk.Combobox(self.gui, textvariable=self.cb_value, width=30)
        self.selection.grid(row=4, column=1)
        self.selection['values'] = ('Najlepszych osobników', 'Selekcja kołem ruletki', 'Selekcja turniejowa')
        self.selection.current(0)

        # crossing
        tk.Label(self.gui, text="Metoda Krzyżowania").grid(row=6, sticky='W')
        self.cb_value_crossing = tk.StringVar()
        self.selection = ttk.Combobox(self.gui, textvariable=self.cb_value_crossing, width=30)
        self.selection.grid(row=6, column=1)
        self.selection['values'] = ('Arytmetyczne', 'Heurystyczne')
        self.selection.current(0)

        # mutation
        tk.Label(self.gui, text="Metoda Mutacji").grid(row=8, sticky='W')
        self.cb_value_mutation = tk.StringVar()
        self.selection = ttk.Combobox(self.gui, textvariable=self.cb_value_mutation, width=30)
        self.selection.grid(row=8, column=1)
        self.selection['values'] = ('Równomierna', 'Zamiana indeksów')
        self.selection.current(0)

        #time

        self.gui.config(menu=self.menubar)
        self.gui.mainloop()

    # events
    def confirm_settings(self):
        self.conf = Configuration()
        self.conf.minimize = bool(self.check_variable.get())
        self.conf.population_size = int(self.population_size.get())
        self.conf.eveluation_size = int(self.eveluation_size.get())
        self.conf.selection = self.cb_value.get()
        if self.cb_value.get() == 'Najlepszych osobników':
            self.conf.selection_size = float(self.selection_conf.get())
        elif self.cb_value.get() == 'Selekcja kołem ruletki':
            self.conf.roulette_probability = float(self.selection_conf.get())
        else:
            self.conf.selection_tournament_size = int(self.selection_conf.get())
        self.conf.crossing_probability = float(self.crossing_conf.get())
        self.conf.mutation_probability = float(self.mutation_conf.get())
        # self.conf.count_members = int(self.count_members.get())

    def start_algorithm(self):
        csv_file = []
        csv_file.append(["Generacja", "y śr", "najlepsze rozwiązanie"])
        self.confirm_settings()
        self.helper = []
        self.average_values = []
        # population
        gen = Population(self.conf)
        population = gen.generate_population()

        tk.Label(self.gui, text="Czas rozpoczęcia obliczeń: ").grid(row=12, column=0, sticky='W')
        tk.Label(self.gui, text=datetime.datetime.now().time()).grid(row=12, column=1, sticky='W')
        self.helper = []
        for k in range(0, self.conf.eveluation_size):
            # selection
            sel = Selection(self.conf)
            if self.cb_value.get() == 'Najlepszych osobników':
                selection_result = sel.ranking_selection(population)
            elif self.cb_value.get() == 'Selekcja kołem ruletki':
                selection_result = sel.roulette_selection(population)
            else:
                selection_result = sel.tournament_selection(population)

            cross = Cross(self.conf)
            after_crossing_population = []

            if self.cb_value_crossing.get() == 'Arytmetyczne':
                after_crossing_population = cross.arithmetic_crossover(selection_result)
            elif self.cb_value_crossing.get() == 'Heurystyczne':
                after_crossing_population = cross.heurestic_crossover(selection_result)

            # mutation
            mut = Mutation(self.conf)
            after_mutation_chromosomes = []
            if self.cb_value_mutation.get() == 'Równomierna':
                after_mutation_chromosomes = mut.even_mutation(after_crossing_population)
            elif self.cb_value_mutation.get() == 'Zamiana indeksów':
                after_mutation_chromosomes = mut.replace_index_mutation(after_crossing_population)
            population = after_mutation_chromosomes

            if self.conf.minimize:
                best = min(population, key=lambda e: e.y).y
                self.helper.append(best)
                print(min(population, key=lambda e: e.y).y)
                print('min')
            else:
                best = max(population, key=lambda e: e.y).y
                print(max(population, key=lambda e: e.y).y)
                print('max')
                self.helper.append(best)

            self.average_values.append(sum(e.y for e in population)/len(population))
            csv_file.append([str(k), str(sum(e.y for e in population)/len(population)), str(best)])
        with open('results.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(csv_file)


        iteration = []
        function_values = []
        for i in range(0, len(self.helper)):
            iteration.append(i)
            function_values.append(self.helper[i])
        import matplotlib.pyplot as plt
        plt.plot(iteration, self.helper)
        plt.xlabel("Iteracja")
        plt.ylabel("Wartość funkcji")
        if self.conf.minimize:
            plt.title("Wykres minimalnych wartości funkcji")
        else:
            plt.title("Wykres maksymalnych wartości funkcji")
        plt.show()

        best_solution = str(min(self.helper)) if self.conf.minimize else str(max(self.helper))

        plt.plot(iteration, self.average_values)
        plt.xlabel("Iteracja")
        plt.ylabel("Średnia wartość funkcji")
        plt.title("Wykres średnich wartości funkcji")
        plt.show()

        tk.Label(self.gui, text="Czas zakończenia obliczeń: ").grid(row=13, column=0, sticky='W')
        tk.Label(self.gui, text=datetime.datetime.now().time()).grid(row=13, column=1, sticky='W')
        tk.Label(self.gui, text='Najlepsze rozwiązanie').grid(row=14, column=0, sticky='W')
        tk.Label(self.gui, text=best_solution).grid(row=14, column=1, sticky='W')


if __name__ == '__main__':
    window = MainWindow()
    window.show()
