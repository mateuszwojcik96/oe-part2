
class Configuration:
    # optimizing -> True - minimize, False - maximizie
    minimize = True
    # population
    population_size: int
    # evalutation
    eveluation_size: int
    # function config
    min_number_range = -10
    max_number_range = 10
    # binary precission
    precision: int
    # selection
    selection: str
    selection_type = {"best" : 1, "roulette" : 2, "rank" : 3}
    roulette_probability: float
    ## best selection
    selection_size: float
    ## tournament selection
    selection_tournament_size: int
    # crossing
    crossing_probability: float
    crossing_heuretic_argument_iterations = 10
    # mutation
    mutation_probability: float
    # elite strategy
    count_members: int