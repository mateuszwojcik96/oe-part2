
class Chromosome:

    def __init__(self, x1_no, x2_no, y):
        self.x1_no = x1_no
        self.x2_no = x2_no
        self.y = y
        self.fitness_probability: float
        self.cumulative_distribution: float