from Model.chromosome import Chromosome
from Presenter.function.booth import Booth
import random

from config import Configuration


class Population:
    def __init__(self, conf):
        self.conf = conf

    def generate_population(self):
        population = []
        for i in range(0, self.conf.population_size):
            x1 = random.randint(-10, 10)
            x2 = random.randint(-10, 10)
            booth = Booth(self.conf.minimize)
            y = booth.function_implementation(x1, x2)
            chromosome = Chromosome(x1, x2, y)
            population.append(chromosome)
        return population


if __name__ == '__main__':
    conf = Configuration()
    conf.population_size = 20
    pop = Population(conf)
    population = pop.generate_population()
    print(population)