from deap import base
from deap import creator
from deap import tools
import random

class Optimize:

    def __init__(self, minimize):
        self.minimize = minimize;

    def individual(icls):
        genome = list()
        genome.append(random.uniform(-10, 10))
        genome.append(random.uniform(-10, 10))
        return icls(genome)

    def function_implementation(self, individual):
        part1 = (individual[0] + 2 * individual[1] - 7) ** 2
        part2 = (2 * individual[0] + individual[1] - 5) ** 2
        if self.minimize:
            return part1 + part2
        else:
            return -(part1 + part2)




if __name__=='__main__':
    opt = Optimize()
    result_individual = opt.individual()
    minimize = True
    if minimize:
        creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    else:
        creator.create("FitnessMax", base.Fitness, weights=(1.0,))
        creator.create("Individual", list, fitness=creator.FitnessMax)

    toolbox = base.Toolbox()
    toolbox.register('individual', opt.individual, creator.Individual)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", opt.function_implementation)

    toolbox.register("select", tools.selTournament, tournsize=3)
    toolbox.register("select", tools.selRandom)
    toolbox.register("select", tools.selBest)
    toolbox.register("select", tools.selWorst)
    toolbox.register("select", tools.selRoulette)

    toolbox.register("crossover", tools.cxTwoPoint)
    toolbox.register("crossover", tools.cxOnePoint)
    toolbox.register("crossover", tools.cxUniform)

    toolbox.register("mate", tools.mutGaussian, mu=5, sigma=10)
    toolbox.register("mate", tools.mutShuffleIndexes, mu=5, sigma=10)
    toolbox.register("mate", tools.mutFlipBit, mu=5, sigma=10)

    sizePopulation = 100
    probabilityMutation = 0.2
    probabilityCrossover = 0.8
    numberIteration = 100

    pop = toolbox.population(n=sizePopulation)
    fitnesses = list(map(toolbox.evaluate, pop))
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit

    g = 0
    numberElitism = 1
    while g < numberIteration:
        g = g + 1
        print("-- Generation %i --" % g)
        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop))
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))
        listElitism = []
        for x in range(0, numberElitism):
            listElitism.append(tools.selBest(pop, 1)[0])
        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
        # cross two individuals with probability CXPB
        if random.random() < probabilityCrossover:
            toolbox.mate(child1, child2)
        # fitness values of the children
        # must be recalculated later
        del child1.fitness.values
        del child2.fitness.values
        for mutant in offspring:
        # mutate an individual with probability MUTPB
        if random.random() < probabilityMutation:
            toolbox.mutate(mutant)
        del mutant.fitness.values
        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
        print(" Evaluated %i individuals" % len(invalid_ind))
        pop[:] = offspring + listElitism
        # Gather all the fitnesses in one list and print the stats
        fits = [ind.fitness.values[0] for ind in pop]
        length = len(pop)
        mean = sum(fits) / length
        sum2 = sum(x * x for x in fits)
        std = abs(sum2 / length - mean ** 2) ** 0.5
        print(" Min %s" % min(fits))
        print(" Max %s" % max(fits))
        print(" Avg %s" % mean)
        print(" Std %s" % std)
        best_ind = tools.selBest(pop, 1)[0]
        print("Best individual is %s, %s" % (best_ind,
                                             best_ind.fitness.values))
    #
    print("-- End of (successful) evolution --") 